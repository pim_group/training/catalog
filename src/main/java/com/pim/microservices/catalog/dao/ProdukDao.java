package com.pim.microservices.catalog.dao;

import com.pim.microservices.catalog.entity.Produk;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProdukDao extends PagingAndSortingRepository<Produk, String>{

}
