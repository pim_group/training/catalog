package com.pim.microservices.catalog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pim.microservices.catalog.dao.ProdukDao;
import com.pim.microservices.catalog.entity.Produk;

@RestController
public class ProdukController {
	
	@Autowired private ProdukDao produkDao;
	
	@GetMapping ("/produk/")
	public Page<Produk> dataProduk(Pageable page){
		return produkDao.findAll(page);
	}
}
